package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N,S,V,i=1,k=0,max;

        N=scanner.nextInt();
        max=0;
        while (i<=N)
        {
            V=scanner.nextInt();
            S=scanner.nextInt();
            if (max<V && S==1)
            {
                k=i;
                max=V;
            }
            i++;
        }
        if (k==0)
        {
            k=-1;
        }
        System.out.println(k);
    }
}
